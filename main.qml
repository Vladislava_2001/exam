import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtMultimedia 5.14

ApplicationWindow {
    visible: true
    width: 600
    height: 800
    title: "Лента новостей"
    property int track_index:-1
    SwipeView {
        id: swipeView
        anchors.fill: parent
        Page{
            id: page1
            header:
                ToolBar{
                anchors.top: parent.top
                background: Rectangle{
                    implicitHeight: 50
                    width: parent.width
                    color: "#313A46"
                    Image{
                        id:hmg2

                        anchors.right: txt2.left
                        anchors.rightMargin: 6
                        width: 30
                        height: 30
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    Text {
                        id: txt2
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.family: "Helvetica"
                        font.pointSize: 16
                        color: "#ffffff"
                        text: "Новости"
                    }
                }
            }


            ListModel{
                id: mod1
                ListElement{
                    isrc: "media/1.jpg"
                    aut: "Московский Политех на выставке"
                    son: "24/11/2021"
                    //path: "C:/Projects/QtExam/181-331_Belov/media/Glitch-Obsidian.mp3"
                }
                ListElement{
                    isrc: "media/2.jpg"
                    aut: "Международная конференция"
                    son: "24/11/2021"
                    //path: "C:/Projects/QtExam/181-331_Belov/media/Helkimer-Back.mp3"
                }
                ListElement{
                    isrc: "media/3.jpg"
                    aut: "Международный конкурс"
                    son: "23/11/2021"
                    //path: "C:/Projects/QtExam/181-331_Belov/media/Mixaund-Team.mp3"
                }
                ListElement{
                    isrc: "media/4.jpg"
                    aut: "Кубок мира по пауэрлифтингу"
                    son: "23/11/2021"
                    //path: "C:/Projects/QtExam/181-331_Belov/media/Punch-Journey.mp3"
                }
                ListElement{
                    isrc: "media/5.jpg"
                    aut: "Фестиваль 'Первокурсник'"
                    son: "23/11/2021"
                    tex: "aaa"
                }
            }



            ListView
            {
                id: lst1
                width: parent.width*0.9
                height: parent.height*0.9
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.margins: 10
                spacing: 10
                model: mod1
                delegate: Rectangle{
                    width: parent.width
                    height: 120
                    color: "#D0313A46"
                    Image {
                        id: imgmod
                        source: isrc
                        anchors.left: parent.left
                        anchors.top: parent.top
                        anchors.margins: 5
                        //height: parent.height*0.8
                        height: 90
                        width: 110
                        fillMode: Image.PreserveAspectFit
                    }
                    Text{
                        id: authorm
                        text: aut
                        anchors.margins: 5
                        anchors.left: imgmod.right
                        anchors.top: parent.top
                        color: "white"
                        font.family: "Helvetica"
                        font.pointSize: 12

                    }
                    Text{
                        id: song
                        text: son
                        anchors.margins: 5
                        anchors.left: imgmod.right
                        anchors.top: authorm.bottom
                        color: "white"
                        font.family: "Helvetica"
                        font.pointSize: 12
                    }

                    Button{
                        id: playb
                        height: 40
                        width: 60
                        anchors.left: imgmod.right
                        anchors.top: song.bottom
                        anchors.margins: 10
                        background: Rectangle{
                            color: "white"
                            Text{
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.verticalCenter: parent.verticalCenter
                                text: "Read"
                                font.family: "Helvetica"
                                font.pointSize: 12
                            }
                        }
                        onClicked:{
                            track_index = index;
                            swipeView.currentIndex=1;
                            img22.source = isrc;
                            texx.source = tex;
                            play.play();
                        }
                    }

                }
            }
        }

        Page{
            id: page2
            header:
                ToolBar{
                anchors.top: parent.top
                background: Rectangle{
                    implicitHeight: 50
                    width: parent.width
                    color: "#313A46"
                    Image{
                        id:hmg1

                        anchors.right: txt1.left
                        anchors.rightMargin: 6
                        width: 60
                        height: 60
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    Text {
                        id: txt1
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.family: "Helvetica"
                        font.pointSize: 16
                        color: "#ffffff"
                        text: "Статья"
                    }

                    Button{
                        id: btn61
                        width: 70
                        implicitHeight: 35
                        anchors.top: parent.top
                        anchors.right: parent.right
                        anchors.margins: 5
                        Text{
                            text: "Назад"
                            color: "black"
                            font.pointSize: 10
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                        }
                        background: Rectangle{
                            width: parent.width
                            height: parent.height
                            radius: 5
                            color: "white"
                        }
                        onClicked:
                        {
                            play.stop();
                            swipeView.currentIndex = 0;
                        }
                    }
                }

            }
            Text{
                id: texx
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: img22.bottom
                color: "black"
                font.family: "Helvetica"
                font.pointSize: 12

            }


           /* Text {
                id: texx
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.margins: 10
                height: parent.height*0.5
                width: parent.width*0.5
                //fillMode: Image.PreserveAspectFit
                MouseArea {
                    width: parent.width
                    height: parent.height
                    preventStealing: true
                    property real velocity: 0.0
                    property int xStart: 0
                    property int xPrev: 0
                    property bool tracing: false
                    onPressed: {
                        xStart = mouse.x
                        xPrev = mouse.x
                        velocity = 0
                        tracing = true
                    }
                    onPositionChanged: {
                        if ( !tracing ) return
                        var currVel = (mouse.x-xPrev)
                        velocity = (velocity + currVel)/2.0
                        xPrev = mouse.x
                        if ( velocity > 15 && mouse.x > parent.width*0.2 ) {
                            tracing = false
                        }
                        else if (velocity < -15 && mouse.x < parent.width*0.2) {
                            tracing = false
                        }
                    }
                    onReleased: {
                        tracing = false
                        if ( velocity > 15 && mouse.x > parent.width*0.2 ) {
                            // SWIPE DETECTED
                            if (track_index===0)
                            {track_index=mod1.count-1;}
                            else{track_index--;}
                            play.stop();
                            img22.source=mod1.get(track_index).isrc;
                            play.source=mod1.get(track_index).tex;
                            play.play();
                        }
                        else if (velocity < -15 && mouse.x < parent.width*0.2) {
                            if(track_index===mod1.count-1){track_index = 0;}
                            else{track_index++;}
                            play.stop();
                            img22.source=mod1.get(track_index).isrc;
                            play.source=mod1.get(track_index).tex;
                            play.play();
                            // SWIPE DETECTED
                        }
                    }
                }
            }*/

            Image {
                id: img22
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.margins: 10
                height: parent.height*0.5
                width: parent.width*0.5
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    width: parent.width
                    height: parent.height
                    preventStealing: true
                    property real velocity: 0.0
                    property int xStart: 0
                    property int xPrev: 0
                    property bool tracing: false
                    onPressed: {
                        xStart = mouse.x
                        xPrev = mouse.x
                        velocity = 0
                        tracing = true
                    }
                    onPositionChanged: {
                        if ( !tracing ) return
                        var currVel = (mouse.x-xPrev)
                        velocity = (velocity + currVel)/2.0
                        xPrev = mouse.x
                        if ( velocity > 15 && mouse.x > parent.width*0.2 ) {
                            tracing = false
                        }
                        else if (velocity < -15 && mouse.x < parent.width*0.2) {
                            tracing = false
                        }
                    }
                    onReleased: {
                        tracing = false
                        if ( velocity > 15 && mouse.x > parent.width*0.2 ) {
                            // SWIPE DETECTED
                            if (track_index===0)
                            {track_index=mod1.count-1;}
                            else{track_index--;}
                            play.stop();
                            img22.source=mod1.get(track_index).isrc;
                            texx.source=mod1.get(track_index).tex;
                            play.play();
                        }
                        else if (velocity < -15 && mouse.x < parent.width*0.2) {
                            if(track_index===mod1.count-1){track_index = 0;}
                            else{track_index++;}
                            play.stop();
                            img22.source=mod1.get(track_index).isrc;
                            texx.source=mod1.get(track_index).tex;
                            play.play();
                            // SWIPE DETECTED
                        }
                    }
                }
            }


            MediaPlayer {
                id: play
                onPositionChanged: {
                    sld.sync = true
                    sld.value = play.position
                    sld.sync = false
                }}





}




        Drawer{
            id: dr1
            width: parent.width * 0.5
            height: parent.height
            dragMargin: 50
            Rectangle{
                anchors.fill: parent
                color: "#A0FFFFFF"
            }
            Image{
                id: imgpol
                width: parent.width * 0.7
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                source: "media/polylogo.jpg"
                fillMode: Image.PreserveAspectFit
            }
            Text {
                id: drtxt11
                color: "black"
                text: "Лента новостей"
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.margins: 10
                width: parent.width*0.9
                font.family: "Helvetica"
                font.pointSize: 16
            }
            Text {
                id: drtxt1
                color: "black"
                text: "Экзаменационное задание по дисциплине \"Разработка безопасных мобильных приложений\". Московский Политех, 24.11.2021"
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.left: parent.left
                anchors.top: drtxt11.bottom
                anchors.margins: 10
                width: parent.width*0.9
                wrapMode: TextArea.WordWrap
                font.family: "Helvetica"
                font.pointSize: 12
            }
            Text {
                id: hyperlink
                color: "black"
                text: "Repo: <a href='https://gitlab.com/Vladislava_2001/exam'>this link</a>"
                onLinkActivated: Qt.openUrlExternally("https://gitlab.com/Vladislava_2001/exam")
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.margins: 10
                horizontalAlignment: Text.AlignRight
                width: parent.width*0.9
                font.family: "Helvetica"
                font.pointSize: 12
            }
            Text{
                id: author
                color: "black"
                text: "Автор: vlada.goncharovagv@yandex.ru"
                anchors.right: parent.right
                anchors.bottom: hyperlink.top
                anchors.margins: 10
                horizontalAlignment: Text.AlignRight
                width: parent.width*0.9
                font.family: "Helvetica"
                font.pointSize: 12
            }
        }
    }

}

